# damianskrzypczak.gitlab.io

Welcome to the damianskrzypczak.gitlab.io blog repository!.

## Article properties

This properties must be added as first lines of article

    Title: My title                                 
    Slug: my-post                                   
    Time: 10 min                                        
    Date: 2010-12-03 10:20      
    Modified: 2010-12-05 19:30
    Category: Python
    Tags: pelican, publishing
    Authors: Alexis Metaireau, Conan Doyle
    Summary: Short version for index and feeds
    Header_Cover: /images/posts/title/cover.png
    Og_Image: http://example.com/facebook_cover.png
    Twitter_Image: http://example.com/twitter_cover.png
  

## Blogging tools

Reading time estimate: [READ-O-METER](http://niram.org/read/),
Choosing tags: [Word Counter](https://wordcounter.io/)  
 
## Draft

If article is not ready to publish, you can just make it as a draft.  
Add `Status: draft` to article properties. Draft article will be still created but will be not visible as public.
All _draft_ articles will be rendered to /draft directory.

## Theme

Theme is custom [Pelican clean blog](https://github.com/gilsondev/pelican-clean-blog) 
created by [Gilson Filho](https://github.com/gilsondev) 
which is based on [Bootstrap Clean Blog](https://startbootstrap.com/template-overviews/clean-blog/)

## Engine

Blog is powered by [Pelican](), and here is [Pelican documentation](http://docs.getpelican.com/en/3.6.3/index.html)

#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Damian Skrzypczak'
SITENAME = u"The Humble Programmer"
SITESUBTITLE = u'Damian Skrzypczak'
SITEURL = ''  # ''damianskrzypczak.gitlab.io'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Warsaw'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('github', 'https://github.com/DamianSkrzypczak'),
          ('gitlab', 'https://gitlab.com/DamianSkrzypczak'),
          ('twitter', 'https://twitter.com/Yoru94'),
          ('linkedin',
           'https://www.linkedin.com/in/damian-skrzypczak-12411b149/'),
          ('envelope', 'mailto:damian.piotr.skrzypczak@gmail.com'))

FAVICON = 'images/favicon.ico'
DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = "theme/clean-blog"

HEADER_COVER = 'images/pylogo.png'
HEADER_COLOR = 'black'

MENUITEMS = [('Home', '/'),
             ('Articles', '/archives.html'),
             ('Categories', '/categories.html'),
             ('Resume', '/files/curriculum_vitae.pdf'),
             # ('Projects', '/pages/projects.html'),
             # ('About Me', '/pages/about-me.html'),
             ('RSS', '/feeds/all.atom.xml')]

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

STATIC_PATHS = ['images', 'files']
Title: Python: afterparty 2. Building blog using pelican and gitlab pages
Headline: Python: afterparty
Slug: python-afterparty-building-blog-using-pelican-and-gitlab-pages
Time: 
Date: 2022-01-01 00:00
Category: Python: afterparty
Tags: python
Authors: Damian Skrzypczak
Summary: Make your own blog using Pelican and gitlab pages!
Header_cover: images/posts/head/sun_shell.png
Status: draft
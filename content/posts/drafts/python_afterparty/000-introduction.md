Title: Series introduction - Python: afterparty
Headline: Python: afterparty
Slug: python-afterparty-introduction
Time: 1 min
Date: 2019-01-06 23:00
Category: Python: afterparty
Tags: python
Authors: Damian Skrzypczak
Summary: Welcome to Python: afterparty series!
Header_cover: images/posts/head/party.png


### Welcome at afterparty!

In this series I will write about tools which can be interesting for someone 
who already know basics (and sa basics I mean general general concept 
about operating with the language itself). You can expect brief look over 
tools like Sphinx or Jupyter, libraries like Selenium or Lxml, languages like 
XPath or Regex and good practices like unit testing or refactoring. 

All mentioned things and more will be available to read in upcoming posts!

Let me know if you have some interesting topic that can be described in 
this series! 



#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
import time

sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://damianskrzypczak.gitlab.io'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
# CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing
DISQUS_SITENAME='humbleprogrammer'
GOOGLE_ANALYTICS = 'UA-131466723-1'

try:
    COMMIT_MSG = open('.git/COMMIT_EDITMSG').read()
except IOError:
    COMMIT_MSG = os.environ['CI_COMMIT_MESSAGE']

COMMIT_DATE = time.ctime(time.time())

COPY_DATE = "2018 - 2019"
